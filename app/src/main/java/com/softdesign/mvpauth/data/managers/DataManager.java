package com.softdesign.mvpauth.data.managers;

import android.content.Context;

import com.softdesign.mvpauth.data.network.RestService;
import com.softdesign.mvpauth.data.storage.dto.AddressDTO;
import com.softdesign.mvpauth.data.storage.dto.ProductDTO;
import com.softdesign.mvpauth.data.storage.dto.UserProfileDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.components.DaggerDataManagerComponent;
import com.softdesign.mvpauth.di.components.DataManagerComponent;
import com.softdesign.mvpauth.di.modules.LocalModule;
import com.softdesign.mvpauth.di.modules.NetworkModule;
import com.softdesign.mvpauth.utils.App;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class DataManager {

    private Context mContext;
    @Inject
    PreferencesManager mPreferencesManager;
    @Inject
    RestService mRestService;
    private List<ProductDTO> mMockProductList;
    private List<AddressDTO> mMockAddressList = new ArrayList<AddressDTO>();
    private UserProfileDTO mMockUserProfile = new UserProfileDTO();
    private Map<Integer, Integer> mMockOrders = new HashMap<>();
    private boolean mAuth = false;

    public DataManager() {
        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if(component == null){
            component = DaggerDataManagerComponent.builder()
                    .appComponent(App.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registertComponent(DataManagerComponent.class, component);
        }
        component.inject(this);

        mContext = App.getContext();
        generateMockData();
    }

    public boolean isAuthUser(){
        return mAuth;
        /*if(!mPreferencesManager.getAuthToken().equals("null")){
            return true;
        }else{
            return true;
        }*/
    }

    public void loginUser(String email, String password){
        mAuth = true;
    }

    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public ProductDTO getProductById(int productId) {
        return mMockProductList.get(productId);
    }

    public void updateProduct(ProductDTO product) {
    }

    public List<ProductDTO> getProductList(){
        return mMockProductList;
    }

    public UserProfileDTO getUserProfileInfo() {
        return mMockUserProfile;
    }

    public void addAddress(AddressDTO addressDTO) {
        mMockAddressList.add(addressDTO);
    }

    public int getCountOrders(){
        int count = 0;
        for(Integer id: mMockOrders.keySet()){
            count += mMockOrders.get(id);
        }
        return count;
    }

    public void addOrder(int orderId, int quantity){
        if(mMockOrders.containsKey(orderId)) {
            mMockOrders.put(orderId, mMockOrders.get(orderId) + quantity);
        }else{
            mMockOrders.put(orderId, quantity);
        }
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<ProductDTO>();
        mMockProductList.add(new ProductDTO(1, "Sid Meier’s Civilization VI", "http://squarefaction.ru/files/game/12369/gallery/20160511185700_73ea7872.jpg", "description description description", 100, 1));
        mMockProductList.add(new ProductDTO(2, "Battlefield 1", "http://blogs-images.forbes.com/insertcoin/files/2016/05/BF1_KEYART_FINAL_1920x1080-1200x675.jpg", "description description description", 200, 1));
        mMockProductList.add(new ProductDTO(3, "zombie vs plants garden warfare 2", "http://oradio.rs/files/uploads/2016/05/plants-vs-zombies-garden-warfare-08-700x393.jpg", "description description description", 300, 1));
        mMockProductList.add(new ProductDTO(4, "witcher 3 blood and wine", "http://www.nim.ru/userfiles/images/The-Witcher-3-blood_wine.jpg", "description description description", 400, 1));
        mMockProductList.add(new ProductDTO(5, "Assassin’s Creed Syndicate 5", "http://androtransfer.com/wp-content/uploads/2015/11/Assassins-Creed-Syndicate-Austin-Wintory-original-images.jpg", "description description description", 500, 1));
        mMockProductList.add(new ProductDTO(6, "Far Cry Primal", "http://farcry5news.com/wp-content/uploads/2015/10/Far-Cry-Primal-Cover-Poster.jpg", "description description description", 600, 1));
        mMockProductList.add(new ProductDTO(7, "Don’t Starve", "http://bentobyte.co/wp-content/uploads/2014/10/dont-starve.jpg", "description description description", 700, 1));
        mMockProductList.add(new ProductDTO(8, "Trine 3", "http://www.virtualreality24.ru/wp-content/uploads/2015/09/trine-3-PC.jpg", "description description description", 800, 1));

        mMockAddressList = new ArrayList<AddressDTO>();
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setNameDeliveryLocation("Дом");
        addressDTO.setStreet("пр.Мира");
        addressDTO.setApartmentNumber("12");
        addressDTO.setHouseNumber("3");
        addressDTO.setFloorNumber("2");
        addressDTO.setComment("после 10");
        mMockAddressList.add(addressDTO);

        mMockUserProfile = new UserProfileDTO();
        mMockUserProfile.setFullName("Gimli");
        mMockUserProfile.setPhone("+9(123)123-12-12");
        mMockUserProfile.setCoverUrl("http://disgustingmen.com/wp-content/uploads/2013/12/the-hobbit-the-desolation-of-smaug.jpg");
        mMockUserProfile.setAvatarUrl("http://3.bp.blogspot.com/-tYT-yaIwpPg/VVNwHMBFouI/AAAAAAAAIiM/0y_YseVGRwo/s1600/gimli_by_vela_s-d8fryoo.png");
        mMockUserProfile.setNoticePromotions(true);
        mMockUserProfile.setNoticeStatusOrder(false);
        mMockUserProfile.setAddressDTOList(mMockAddressList);
    }
}
