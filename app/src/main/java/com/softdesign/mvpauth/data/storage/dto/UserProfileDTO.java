package com.softdesign.mvpauth.data.storage.dto;

import java.util.List;

public class UserProfileDTO {
    private String fullName;
    private String coverUrl;
    private String avatarUrl;
    private String phone;
    private List<AddressDTO> addressDTOList;
    private boolean noticeStatusOrder;
    private boolean noticePromotions;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<AddressDTO> getAddressDTOList() {
        return addressDTOList;
    }

    public void setAddressDTOList(List<AddressDTO> addressDTOList) {
        this.addressDTOList = addressDTOList;
    }

    public boolean isNoticeStatusOrder() {
        return noticeStatusOrder;
    }

    public void setNoticeStatusOrder(boolean noticeStatusOrder) {
        this.noticeStatusOrder = noticeStatusOrder;
    }

    public boolean isNoticePromotions() {
        return noticePromotions;
    }

    public void setNoticePromotions(boolean noticePromotions) {
        this.noticePromotions = noticePromotions;
    }
}
