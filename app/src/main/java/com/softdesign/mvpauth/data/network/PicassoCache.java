package com.softdesign.mvpauth.data.network;

import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

public class PicassoCache {
    private static Picasso mPicassoInstance;

    private PicassoCache(Context context) {
        OkHttp3Downloader okHttp3Downloader = new OkHttp3Downloader(context, Integer.MAX_VALUE);
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(okHttp3Downloader);
        mPicassoInstance = builder.build();
        Picasso.setSingletonInstance(mPicassoInstance);
    }

    public static Picasso getPicassoInstance(Context context){
        if (mPicassoInstance == null){
            new PicassoCache(context);
            return mPicassoInstance;
        }
        return mPicassoInstance;
    }
}
