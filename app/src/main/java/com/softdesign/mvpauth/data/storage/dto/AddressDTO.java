package com.softdesign.mvpauth.data.storage.dto;

public class AddressDTO {

    private String mNameDeliveryLocation;
    private String mStreet;
    private String mHouseNumber;
    private String mApartmentNumber;
    private String mFloorNumber;
    private String mComment;

    public AddressDTO() {
    }

    public String getNameDeliveryLocation() {
        return mNameDeliveryLocation;
    }

    public void setNameDeliveryLocation(String nameDeliveryLocation) {
        mNameDeliveryLocation = nameDeliveryLocation;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        mStreet = street;
    }

    public String getHouseNumber() {
        return mHouseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        mHouseNumber = houseNumber;
    }

    public String getApartmentNumber() {
        return mApartmentNumber;
    }

    public void setApartmentNumber(String apartmentNumber) {
        mApartmentNumber = apartmentNumber;
    }

    public String getFloorNumber() {
        return mFloorNumber;
    }

    public void setFloorNumber(String floorNumber) {
        mFloorNumber = floorNumber;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }
}
