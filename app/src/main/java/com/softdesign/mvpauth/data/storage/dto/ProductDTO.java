package com.softdesign.mvpauth.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductDTO implements Parcelable{

    private int id;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    private int count;

    public ProductDTO(int id, String productName, String imageUrl, String description,int price, int count) {
        this.id = id;
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.description = description;
        this.count = count;
        this.price = price;
    }

    //region =================== Parcelable ===================
    protected ProductDTO(Parcel in) {
        id = in.readInt();
        productName = in.readString();
        imageUrl = in.readString();
        description = in.readString();
        count = in.readInt();
        price = in.readInt();
    }

    public static final Creator<ProductDTO> CREATOR = new Creator<ProductDTO>() {
        @Override
        public ProductDTO createFromParcel(Parcel in) {
            return new ProductDTO(in);
        }

        @Override
        public ProductDTO[] newArray(int size) {
            return new ProductDTO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(productName);
        parcel.writeString(imageUrl);
        parcel.writeString(description);
        parcel.writeInt(count);
        parcel.writeInt(price);
    }
    //endregion

    //region =================== Getters ===================
    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public int getCount() {
        return count;
    }

    public int getPrice() {
        return price;
    }
    //endregion

    public void deleteProduct() {
        count--;
    }

    public void addProduct() {
        count++;
    }
}
