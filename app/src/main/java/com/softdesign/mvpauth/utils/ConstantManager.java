package com.softdesign.mvpauth.utils;

public interface ConstantManager {

    String TAG_PREFIX = "DEV ";

    String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";

    int EXIT_DIALOG = 0;
}
