package com.softdesign.mvpauth.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import com.squareup.picasso.Transformation;

public class PicassoTransformUserAvatar implements Transformation {

    private int mStroke;
    private int mColor;

    public PicassoTransformUserAvatar(int stroke, int color) {
        mStroke = stroke;
        mColor = color;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        return getRoundedBitmapithStroke(source);
    }

    @Override
    public String key() {
        return "transform_key";
    }

    private Bitmap getRoundedBitmapithStroke(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        paint.setColor(Color.RED);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawOval(rectF, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        final Paint paintWhiteOval = new Paint();
        int halfStroke = mStroke / 2;
        final Rect strokeRect = new Rect(halfStroke, halfStroke,
                bitmap.getWidth() - halfStroke, bitmap.getHeight() - halfStroke);
        final RectF strokeRectF = new RectF(strokeRect);

        paintWhiteOval.setAntiAlias(true);
        paintWhiteOval.setColor(mColor);
        paintWhiteOval.setStrokeWidth(mStroke);
        paintWhiteOval.setStyle(Paint.Style.STROKE);
        canvas.drawOval(strokeRectF, paintWhiteOval);

        bitmap.recycle();

        return output;
    }

}
