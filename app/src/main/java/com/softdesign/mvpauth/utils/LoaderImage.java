package com.softdesign.mvpauth.utils;

import android.util.Log;
import android.widget.ImageView;

import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.components.DaggerPicassoComponent;
import com.softdesign.mvpauth.di.components.PicassoComponent;
import com.softdesign.mvpauth.di.modules.PicassoCacheModule;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class LoaderImage {

    private static LoaderImage instanse = new LoaderImage();

    public LoaderImage() {
        PicassoComponent component = createDaggerComponent();
        component.inject(this);
    }

    public static LoaderImage getInstanse() {
        return instanse;
    }

    private static final String TAG = ConstantManager.TAG_PREFIX + "LoaderImage";

    @Inject
    Picasso mPicasso;

    public void setUserAvatar(
            final String uri, final ImageView imageView, final int stroke, final int color){
        mPicasso.load(uri)
                .fit()
                .transform(new PicassoTransformUserAvatar(stroke, color))
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, " user avatar load from cache");
                    }

                    @Override
                    public void onError() {
                        mPicasso.load(uri)
                                .fit()
                                .transform(new PicassoTransformUserAvatar(stroke, color))
                                .into(imageView);
                    }
                });
    }

    public void setUserCover(final String uri, final ImageView imageView){
        mPicasso.load(uri)
                .fit()
                .centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, " user cover load from cache");
                    }

                    @Override
                    public void onError() {
                        mPicasso.load(uri)
                                .fit()
                                .centerCrop()
                                .into(imageView);
                    }
                });
    }

    //region =================== DI ===================

    private PicassoComponent createDaggerComponent(){
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if(picassoComponent == null){
            picassoComponent = DaggerPicassoComponent.builder()
                    .appComponent(App.getAppComponent())
                    .picassoCacheModule(new PicassoCacheModule())
                    .build();
            DaggerService.registertComponent(PicassoComponent.class, picassoComponent);
        }

        return picassoComponent;
    }

    //endregion

}
