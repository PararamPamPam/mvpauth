package com.softdesign.mvpauth.utils.textwatchers;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.utils.App;

public class PasswordTextWatcher implements TextWatcher{

    private EditText mEditText;
    private Context mContext;

    public PasswordTextWatcher(EditText editText) {
        mEditText = editText;
        mContext = App.getContext();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        String text = editable.toString();
        if(!text.matches("[a-zA-Z0-9]+") || text.length() < 8 || text.length() > 16){
            mEditText.setError(mContext.getResources()
                    .getString(R.string.message_incorrect_password));
        }else{
            mEditText.setError(null);
        }
    }
}
