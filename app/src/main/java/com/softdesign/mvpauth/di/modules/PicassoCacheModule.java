package com.softdesign.mvpauth.di.modules;

import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PicassoCacheModule {

    @Provides
    @Singleton
    Picasso providePicasso(Context context){
        OkHttp3Downloader okHttpDownloader = new OkHttp3Downloader(context, Integer.MAX_VALUE);
        Picasso picasso = new Picasso.Builder(context)
                .downloader(okHttpDownloader)
                .build();
        Picasso.setSingletonInstance(picasso);

        return picasso;
    }
}
