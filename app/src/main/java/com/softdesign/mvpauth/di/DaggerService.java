package com.softdesign.mvpauth.di;

import android.support.annotation.Nullable;
import android.util.Log;

import com.softdesign.mvpauth.di.components.DaggerPicassoComponent;
import com.softdesign.mvpauth.di.components.PicassoComponent;
import com.softdesign.mvpauth.di.modules.PicassoCacheModule;
import com.softdesign.mvpauth.utils.App;
import com.softdesign.mvpauth.utils.ConstantManager;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DaggerService {
    private static final String TAG = ConstantManager.TAG_PREFIX + "DaggerService";
    private static Map<Class, Object> sComponentMap = new HashMap<>();

    public static void registertComponent(Class componentClass, Object daggerComponent){
        sComponentMap.put(componentClass, daggerComponent);
    }

    @Nullable
    @SuppressWarnings("unchecked")
    public static <T> T getComponent(Class<T> componentClass){
        return (T) sComponentMap.get(componentClass);
    }

    public static void unregisterScope(Class<? extends Annotation> scopeAnnotation){
        Iterator<Map.Entry<Class, Object>> iterator = sComponentMap.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Class, Object> entry = iterator.next();
            if(entry.getKey().isAnnotationPresent(scopeAnnotation)){
                iterator.remove();
            }
        }
    }

    public static <T> T createDaggercomponent(List<Object> modulesAndComponentList, Class<T> daggerComponentClass){
        Log.d(TAG, "createDaggercomponent " + daggerComponentClass.getSimpleName());
        for(Method method: daggerComponentClass.getDeclaredMethods()){
            if(method.getName().equals("builder")){
                try {
                    Object builder = method.invoke(daggerComponentClass);
                    setModulesAndComponents(modulesAndComponentList, builder);
                    Method methodBuild = builder.getClass().getMethod("build");
                    return (T) methodBuild.invoke(builder);
                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private static void setModulesAndComponents(List<Object> moduleAndComponentList, Object builder){
        Log.d(TAG, "setModulesAndComponents");
        Method[] methods = builder.getClass().getMethods();
        for(int i = 0; i < moduleAndComponentList.size(); i++){

            Object objectForInesrt = moduleAndComponentList.get(i);
            for(int j = 0; j < methods.length; j++){

                Class<?>[] parameterTypes = methods[j].getParameterTypes();
                if(parameterTypes.length == 1 &&
                        parameterTypes[0].isInstance(objectForInesrt) &&
                                parameterTypes[0] != Object.class){
                    try {
                        methods[j].invoke(builder, objectForInesrt);
                    } catch (Exception e) {
                        Log.d(TAG, e.toString());
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }
    }

    public static PicassoComponent getPicassoComponent(){
        PicassoComponent picassoComponent = DaggerService.getComponent(PicassoComponent.class);
        if(picassoComponent == null){
            picassoComponent = DaggerPicassoComponent.builder()
                    .appComponent(App.getAppComponent())
                    .picassoCacheModule(new PicassoCacheModule())
                    .build();
            DaggerService.registertComponent(PicassoComponent.class, picassoComponent);
        }
        return picassoComponent;
    }

}
