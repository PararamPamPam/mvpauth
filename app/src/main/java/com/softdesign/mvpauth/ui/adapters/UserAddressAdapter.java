package com.softdesign.mvpauth.ui.adapters;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.AddressDTO;
import com.softdesign.mvpauth.utils.ConstantManager;

import java.util.List;

public class UserAddressAdapter extends RecyclerView.Adapter<UserAddressAdapter.UserAddressViewHolder>{

    private static final String TAG = ConstantManager.TAG_PREFIX + "UserAddressAdapter";

    private Context mContext;
    private List<AddressDTO> mAddressList;

    public UserAddressAdapter(List<AddressDTO> addressList) {
        mAddressList = addressList;
    }

    @Override
    public UserAddressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View convertView = LayoutInflater.from(mContext).
                inflate(R.layout.item_address_list, parent, false);
        return new UserAddressViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(UserAddressViewHolder holder, int position) {
        holder.mNameAddress.setHint(mAddressList.get(position).getNameDeliveryLocation());
        holder.mAddress.setText(mAddressList.get(position).getStreet());
        holder.mComment.setText(mAddressList.get(position).getComment());
    }

    @Override
    public int getItemCount() {
        return mAddressList.size();
    }

    public static class UserAddressViewHolder extends RecyclerView.ViewHolder {
        private TextInputLayout mNameAddress;
        private EditText mAddress;
        private EditText mComment;

        public UserAddressViewHolder(View itemView) {
            super(itemView);
            mNameAddress = (TextInputLayout) itemView.findViewById(R.id.name_address);
            mAddress = (EditText) itemView.findViewById(R.id.address_et);
            mComment = (EditText) itemView.findViewById(R.id.comment_et);
        }
    }
}
