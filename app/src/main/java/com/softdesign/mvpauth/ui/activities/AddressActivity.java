package com.softdesign.mvpauth.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.softdesign.mvpauth.BuildConfig;
import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.AddressDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AddressScope;
import com.softdesign.mvpauth.mvp.presenters.AddressPresenter;
import com.softdesign.mvpauth.mvp.views.IAddressView;
import com.softdesign.mvpauth.utils.App;
import com.softdesign.mvpauth.utils.textwatchers.AddressTextWatcners;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;

public class AddressActivity extends BaseActivity implements IAddressView, View.OnClickListener{

    @BindView(R.id.delivery_location_et)
    EditText mNameDeliveryLocationEt;
    @BindView(R.id.street_et)
    EditText mStreetEt;
    @BindView(R.id.house_number_et)
    EditText mHouseNumberEt;
    @BindView(R.id.apartment_number_et)
    EditText mApartmentNumberEt;
    @BindView(R.id.floor_number_et)
    EditText mFloorNumberEt;
    @BindView(R.id.comment_et)
    EditText mCommentEt;
    @BindView(R.id.point_on_map_btn)
    Button mPointOnMapBtn;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private List<EditText> mRequiredField = new ArrayList<>();
    private TextView mNotifyCount;

    @Inject
    AddressPresenter mAddressPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        ButterKnife.bind(this);
        daggerInject();

        mAddressPresenter.takeView(this);
        mAddressPresenter.initView();

        initToolbar();

        mPointOnMapBtn.setOnClickListener(this);

        mNameDeliveryLocationEt.addTextChangedListener(new AddressTextWatcners(mNameDeliveryLocationEt));
        mStreetEt.addTextChangedListener(new AddressTextWatcners(mStreetEt));
        mRequiredField.add(mNameDeliveryLocationEt);
        mRequiredField.add(mStreetEt);
    }

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerAddressActivity_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bucket, menu);

        MenuItem item = menu.findItem(R.id.bucket);
        MenuItemCompat.setActionView(item, R.layout.actionbar_bucket_notification);
        RelativeLayout relativeLayout = (RelativeLayout) item.getActionView();
        mNotifyCount = (TextView) relativeLayout.findViewById(R.id.count_notification);

        mAddressPresenter.setupMenu();

        return super.onCreateOptionsMenu(menu);
    }

    //region ================= IRootView =================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if(BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        }else{
            showMessage("Что-то пошло не так");
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public void setOrderNotification(String quantity) {
        if(mNotifyCount != null){
            mNotifyCount.setVisibility(View.VISIBLE);
            mNotifyCount.setText(quantity);
        }
    }

    //endregion

    //region =================== DI ===================

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.point_on_map_btn:
                break;
        }
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.address_fragment_title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mAddressPresenter.clickOnAddAddressDelivery();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public AddressDTO getAddressDelivery(){
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setNameDeliveryLocation(mNameDeliveryLocationEt.getText().toString());
        addressDTO.setStreet(mStreetEt.getText().toString());
        addressDTO.setHouseNumber(mHouseNumberEt.getText().toString());
        addressDTO.setApartmentNumber(mApartmentNumberEt.getText().toString());
        addressDTO.setFloorNumber(mFloorNumberEt.getText().toString());
        addressDTO.setComment(mCommentEt.getText().toString());
        return addressDTO;
    }

    @Override
    public boolean checkInputData(){
        boolean result = true;
        for(EditText editText: mRequiredField){
            if(editText.getText().toString().equals("")){
                editText.setError(App.getContext().getResources()
                        .getString(R.string.message_incorrect_address));
                result = false;
            }
        }
        return result;
    }

    @Override
    public void close(){
        onBackPressed();
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter(){
            return new AddressPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @AddressScope
    interface Component {
        void inject(AddressActivity sctivity);
    }

    //endregion

}
