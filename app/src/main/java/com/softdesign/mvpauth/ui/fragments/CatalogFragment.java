package com.softdesign.mvpauth.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.ProductDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.CatalogScope;
import com.softdesign.mvpauth.mvp.presenters.CatalogPresenter;
import com.softdesign.mvpauth.mvp.views.ICatalogView;
import com.softdesign.mvpauth.ui.adapters.CatalogAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener{

    public static final String NAME = "CatalogFragment";

    @BindView(R.id.product_pager)
    ViewPager mProductPager;
    @BindView(R.id.add_to_card_btn)
    Button mAddToCardBtn;

    @Inject
    CatalogPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this, view);
        daggerInject();

        mPresenter.takeView(this);
        mPresenter.initView();
        mAddToCardBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.dropView();
    }

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerCatalogFragment_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    //region =============== ICatalogView ===============
    @Override
    public void showCatalogView(List<ProductDTO> productList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for(ProductDTO product: productList){
            adapter.addItem(product);
        }
        mProductPager.setAdapter(adapter);
    }

    @Override
    public void showAuthScreen() {
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new AuthFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void updateProductCounter() {
    }
    //endregion

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.add_to_card_btn){
            mPresenter.clickOnBuyButton(mProductPager.getCurrentItem());
        }
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @CatalogScope
        CatalogPresenter provideProductModel(){
            return new CatalogPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @CatalogScope
    interface Component {
        void inject(CatalogFragment fragment);
    }

    //endregion
}
