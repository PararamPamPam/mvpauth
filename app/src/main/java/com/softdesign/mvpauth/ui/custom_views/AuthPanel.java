package com.softdesign.mvpauth.ui.custom_views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.softdesign.mvpauth.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthPanel extends LinearLayout {

    private final static String TAG = "AuthPanel";
    public final static int LOGIN_STATE = 0;
    public final static int IDLE_STATE = 1;
    private int mCustomState = 1;

    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;
    @BindView(R.id.auth_card)
    CardView mAuthCard;

    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.state = mCustomState;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCustomState(savedState.state);
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
        showViewFromState();
    }

    private void showLoginState() {
        mAuthCard.setVisibility(View.VISIBLE);
        mShowCatalogBtn.setVisibility(View.GONE);
    }

    private void showIdleState() {
        mAuthCard.setVisibility(View.GONE);
        mShowCatalogBtn.setVisibility(View.VISIBLE);
    }

    private void showViewFromState() {
        if(mCustomState == LOGIN_STATE){
            showLoginState();
        }else{
            showIdleState();
        }
    }

    public String getUserEmail(){
        return mEmailEt.getText().toString();
    }

    public String getUserPassword(){
        return mPasswordEt.getText().toString();
    }

    public boolean isIdle(){
        return mCustomState == IDLE_STATE;
    }

    static class SavedState extends BaseSavedState {
        private int state;

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>(){

            @Override
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel source) {
            super(source);
            state = source.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
        }
    }
}
