package com.softdesign.mvpauth.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.softdesign.mvpauth.data.storage.dto.ProductDTO;
import com.softdesign.mvpauth.ui.fragments.ProductFragment;

import java.util.ArrayList;
import java.util.List;

public class CatalogAdapter extends FragmentPagerAdapter{

    private List<ProductDTO> mProductList;

    public CatalogAdapter(FragmentManager fm) {
        super(fm);
        mProductList = new ArrayList<ProductDTO>();
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.newInstance(mProductList.get(position));
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    public void addItem(ProductDTO product){
        mProductList.add(product);
        notifyDataSetChanged();
    }
}
