package com.softdesign.mvpauth.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.AddressDTO;
import com.softdesign.mvpauth.data.storage.dto.UserProfileDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.ProfileScope;
import com.softdesign.mvpauth.mvp.presenters.ProfilePresenter;
import com.softdesign.mvpauth.mvp.views.IProfileView;
import com.softdesign.mvpauth.ui.activities.AddressActivity;
import com.softdesign.mvpauth.ui.adapters.UserAddressAdapter;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.LoaderImage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;

public class ProfileFragment extends Fragment implements IProfileView, View.OnClickListener {

    public static final String NAME = "ProfileFragment";
    private static final String TAG = ConstantManager.TAG_PREFIX + "ProfileFragment";

    @BindView(R.id.user_name_txt)
    TextView muserNameTxt;
    @BindView(R.id.cover_img)
    ImageView mCoverImg;
    @BindView(R.id.user_avatar_img)
    ImageView mUserAvatarImg;
    @BindView(R.id.phone_et)
    EditText mUserPhoneEt;
    @BindView(R.id.add_address_btn)
    Button mAddAddressBtn;
    @BindView(R.id.notice_status_order_switch)
    SwitchCompat mNoticeStatusOrderSwitch;
    @BindView(R.id.notice_promotions_switch)
    SwitchCompat mNoticePromotionsSwitch;
    @BindView(R.id.address_list)
    RecyclerView mAddressListView;

    @Inject
    ProfilePresenter mProfilePresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        daggerInject();

        mProfilePresenter.takeView(this);
        mProfilePresenter.initView();

        mAddAddressBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        mProfilePresenter.resume();
        super.onResume();
    }

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerProfileFragment_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_address_btn:
                mProfilePresenter.clickAddAddressBtn();
        }
    }

    //region =================== IProfileView ===================
    @Override
    public void openAddAddressScreen(Activity activity) {
        Intent intent = new Intent(activity, AddressActivity.class);
        startActivity(intent);
    }

    @Override
    public void setUserProfileInfo(UserProfileDTO userProfileInfo) {
        if(userProfileInfo != null){
            muserNameTxt.setText(userProfileInfo.getFullName());
            mUserPhoneEt.setText(userProfileInfo.getPhone());
            mNoticePromotionsSwitch.setChecked(userProfileInfo.isNoticePromotions());
            mNoticeStatusOrderSwitch.setChecked(userProfileInfo.isNoticeStatusOrder());
            LoaderImage.getInstanse().setUserAvatar(
                    userProfileInfo.getAvatarUrl(), mUserAvatarImg, 4, Color.WHITE);
            LoaderImage.getInstanse().setUserCover(userProfileInfo.getCoverUrl(), mCoverImg);
        }
    }

    @Override
    public void setAddress(List<AddressDTO> addressList, Context contex) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(contex);
        mAddressListView.setLayoutManager(linearLayoutManager);
        UserAddressAdapter addressAdapter = new UserAddressAdapter(addressList);
        mAddressListView.setAdapter(addressAdapter);
    }
    //endregion

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @ProfileScope
        ProfilePresenter provideProductModel(){
            return new ProfilePresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @ProfileScope
    interface Component {
        void inject(ProfileFragment fragment);
    }

    //endregion

}
