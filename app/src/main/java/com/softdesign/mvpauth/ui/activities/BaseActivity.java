package com.softdesign.mvpauth.ui.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;

import com.softdesign.mvpauth.R;

public class BaseActivity extends AppCompatActivity{

    protected ProgressDialog mProgressDialog = null;

    public void showProgress(){
        if(mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
            mProgressDialog.setCancelable(false);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 22, 7, 5)));
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        }else{
            mProgressDialog.show();
            mProgressDialog.setContentView(R.layout.progress_splash);
        }
    }

    public void hideProgress(){
        if(mProgressDialog != null) {
            if(mProgressDialog.isShowing()){
                mProgressDialog.hide();
            }
        }
    }

    @Override
    protected void onPause() {
        if(mProgressDialog != null){
            mProgressDialog.dismiss();
        }
        super.onPause();
    }
}
