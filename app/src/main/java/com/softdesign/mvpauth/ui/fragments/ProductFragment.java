package com.softdesign.mvpauth.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.managers.DataManager;
import com.softdesign.mvpauth.data.storage.dto.ProductDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.components.DaggerPicassoComponent;
import com.softdesign.mvpauth.di.components.DaggerPicassoComponent;
import com.softdesign.mvpauth.di.components.PicassoComponent;
import com.softdesign.mvpauth.di.modules.PicassoCacheModule;
import com.softdesign.mvpauth.di.scopes.ProductScope;
import com.softdesign.mvpauth.mvp.presenters.ProductPresenter;
import com.softdesign.mvpauth.mvp.presenters.ProductPresenterFactory;
import com.softdesign.mvpauth.mvp.views.IProductView;
import com.softdesign.mvpauth.ui.activities.RootActivity;
import com.softdesign.mvpauth.ui.custom_views.AspectRatioImageView;
import com.softdesign.mvpauth.utils.App;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Component;
import dagger.Provides;

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener{

    public static final String NAME = "ProductFragment";
    private static final String TAG = ConstantManager.TAG_PREFIX + "ProductFragment";

    @BindView(R.id.product_name_txt)
    TextView mProductNameTxt;
    @BindView(R.id.product_count_txt)
    TextView mProductCountTxt;
    @BindView(R.id.product_description_txt)
    TextView mProductDescriptionTxt;
    @BindView(R.id.product_price_txt)
    TextView mProductPriceTxt;
    @BindView(R.id.product_image)
    AspectRatioImageView mProductImage;
    @BindView(R.id.plus_btn)
    ImageButton mPlusBtn;
    @BindView(R.id.minus_btn)
    ImageButton mMinusBtn;

    @Inject
    Picasso mPicasso;

    @Inject
    ProductPresenter mPresenter;

    public static ProductFragment newInstance(ProductDTO product){
        Bundle bundle = new Bundle();
        bundle.putParcelable("PRODUCT", product);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readBundle(Bundle bundle){
        Log.d(TAG, "readBundle " + this.toString());
        if(bundle != null){
            ProductDTO product = bundle.getParcelable("PRODUCT");
            daggerInject(product);
        }
    }

    private void daggerInject(ProductDTO product){
        List<Object> modulesAndComponentList = new ArrayList<>();
        Collections.addAll(modulesAndComponentList,
                DaggerService.getPicassoComponent(),
                new Module(product));
        Component component = DaggerService.createDaggercomponent(
                modulesAndComponentList, DaggerProductFragment_Component.class);
        component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView " + this.toString());
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        mPlusBtn.setOnClickListener(this);
        mMinusBtn.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView " + this.toString());
        mPresenter.dropView();
        mPresenter = null;
        super.onDestroyView();
    }

    //region ================ IProductView ================
    @Override
    public void showProductView(ProductDTO product) {
        mProductNameTxt.setText(String.valueOf(product.getProductName()));
        mProductDescriptionTxt.setText(String.valueOf(product.getDescription()));
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        setupImageProduct(product.getImageUrl());
        if(product.getCount() > 0){
            mProductPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice()) + ".-");
        }else{
            mProductPriceTxt.setText(String.valueOf(product.getPrice()) + ".-");
        }
    }

    @Override
    public void updateProductCountView(ProductDTO product) {
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if(product.getCount() > 0){
            mProductPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice()) + ".-");
        }
    }
    //endregion

    private RootActivity getRootActivity(){
        return (RootActivity) getActivity();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
        }
    }

    private void setupImageProduct(final String imageUrl){
        Log.d(TAG, "setupImageProduct: " + imageUrl);
        mPicasso.load(imageUrl)
                .error(R.drawable.error)
                .fit()
                .centerCrop()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(mProductImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "Загрузка изображения из кеша прошла успешна");
                    }
                    @Override
                    public void onError() {
                        mPicasso.load(imageUrl)
                                .error(R.drawable.error)
                                .fit()
                                .centerCrop()
                                .into(mProductImage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        Log.d(TAG, "Загрузка изображения из сети прошла успешна");
                                    }
                                    @Override
                                    public void onError() {
                                        Log.d(TAG, "Загрузка изображения из сети не удалась");
                                    }
                                });
                    }
                });
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {
        ProductDTO mProductDTO;

        public Module(ProductDTO productDTO)     {
            mProductDTO = productDTO;
        }

        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter(){
            return new ProductPresenter(mProductDTO);
        }
    }

    @dagger.Component(dependencies = PicassoComponent.class, modules = Module.class)
    @ProductScope
    interface Component {
        void inject(ProductFragment fragment);
    }

    //endregion
}
