package com.softdesign.mvpauth.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AuthScope;
import com.softdesign.mvpauth.mvp.presenters.AuthFragmentPresenter;
import com.softdesign.mvpauth.mvp.views.IAuthFragmentView;
import com.softdesign.mvpauth.ui.custom_views.AuthPanel;
import com.softdesign.mvpauth.utils.textwatchers.EmailTextWatchers;
import com.softdesign.mvpauth.utils.textwatchers.PasswordTextWatcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;

public class AuthFragment extends Fragment implements IAuthFragmentView, View.OnClickListener{

    public static final String NAME = "AuthFragment";

    @BindView(R.id.logo_img)
    ImageView mLogoImg;
    @BindView(R.id.app_name_txt)
    TextView mAppNameTxt;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;
    @BindView(R.id.auth_card)
    CardView mInputDataForm;
    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    @Inject
    AuthFragmentPresenter mAuthFragmentPresenter;

    private String mNextFragmentName;

    public static AuthFragment newInstance(String nameFragmentToGo){
        Bundle bundle = new Bundle();
        bundle.putString("NAME_FRAGMENT", nameFragmentToGo);
        AuthFragment fragment = new AuthFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readBundle(Bundle bundle){
        if(bundle != null){
            mNextFragmentName = bundle.getString("NAME_FRAGMENT", null);
        }
    }

    //region ========================= Life cycle ========================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_splash, container, false);
        ButterKnife.bind(this, view);
        daggerInject();
        readBundle(getArguments());

        mAuthFragmentPresenter.takeView(this);
        mAuthFragmentPresenter.initView();

        mLoginBtn.setOnClickListener(this);
        mEmailEt.addTextChangedListener(new EmailTextWatchers(mEmailEt));
        mPasswordEt.addTextChangedListener(new PasswordTextWatcher(mPasswordEt));

        return view;
    }

    @Override
    public void onResume() {
        mLogoImg.setVisibility(View.GONE);
        mAppNameTxt.setVisibility(View.GONE);
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        mAuthFragmentPresenter.dropView();
        DaggerService.unregisterScope(AuthScope.class);
        super.onDestroyView();
    }
    //endregion

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerAuthFragment_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    //region ======================== AuthView ========================

    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }

    @Override
    public EditText getEmailEditText() {
        return mEmailEt;
    }

    @Override
    public EditText getPasswordEditText() {
        return mPasswordEt;
    }
    //endregion

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.login_btn:
                mAuthFragmentPresenter.clickOnLogin();
                break;
        }
    }

    @Override
    public void goBack() {
        getFragmentManager().popBackStack();
    }

    @Override
    public String getNextFragmetnName() {
        return mNextFragmentName;
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @AuthScope
        AuthFragmentPresenter provideAuthFragmentPresenter(){
            return new AuthFragmentPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {
        void inject(AuthFragment fragment);
    }

    //endregion
}
