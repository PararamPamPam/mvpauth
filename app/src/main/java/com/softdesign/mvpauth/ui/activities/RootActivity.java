package com.softdesign.mvpauth.ui.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.softdesign.mvpauth.BuildConfig;
import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.UserProfileDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.RootScope;
import com.softdesign.mvpauth.mvp.presenters.RootPresenter;
import com.softdesign.mvpauth.mvp.views.IRootView;
import com.softdesign.mvpauth.ui.fragments.AuthFragment;
import com.softdesign.mvpauth.ui.fragments.CatalogFragment;
import com.softdesign.mvpauth.ui.fragments.ProfileFragment;
import com.softdesign.mvpauth.utils.ConstantManager;
import com.softdesign.mvpauth.utils.LoaderImage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;

public class RootActivity extends BaseActivity implements IRootView, NavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = ConstantManager.TAG_PREFIX + "RootActivity";

    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.fragment_container)
    FrameLayout mFragmentContainer;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;

    FragmentManager mFragmentManager;
    private TextView mNotifyCount;

    @Inject
    RootPresenter mRootPresenter;
    private String mCurrenFragmentName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

        ButterKnife.bind(this);
        daggerInject();

        initToolbar();
        initDrawer();
        mRootPresenter.takeView(this);
        mRootPresenter.initView();

        setTitleByNameFragment(CatalogFragment.class.getSimpleName());
        setMenuItemCheckedByNameFragment(CatalogFragment.class.getSimpleName());

        mFragmentManager = getSupportFragmentManager();
        if(savedInstanceState == null){
            mCurrenFragmentName = CatalogFragment.NAME;
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        mRootPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        int countFragmentInBackStack = mFragmentManager.getBackStackEntryCount();

        //Если следующий фрагмент "AuthFragment", то перепрыгиваем его
        while(countFragmentInBackStack > 0 &&
                mFragmentManager.getFragments().get(countFragmentInBackStack - 1) instanceof AuthFragment){
            mFragmentManager.popBackStack();
            mFragmentManager.getFragments().set(countFragmentInBackStack - 1, null);
            countFragmentInBackStack--;
        }

        if(countFragmentInBackStack == 0) {
            showDialog(ConstantManager.EXIT_DIALOG);
        }else{
            String fragmentName = mFragmentManager.getFragments().get(countFragmentInBackStack - 1).getClass().getSimpleName();
            setTitleByNameFragment(fragmentName);
            setMenuItemCheckedByNameFragment(fragmentName);

            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.bucket, menu);

        MenuItem item = menu.findItem(R.id.bucket);
        MenuItemCompat.setActionView(item, R.layout.actionbar_bucket_notification);
        RelativeLayout relativeLayout = (RelativeLayout) item.getActionView();
        mNotifyCount = (TextView) relativeLayout.findViewById(R.id.count_notification);

        mRootPresenter.setupMenu();

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        String nameFragment = null;
        switch (item.getItemId()){
            case R.id.nav_account:
                nameFragment = ProfileFragment.NAME;
                break;
            case R.id.nav_catalog:
                nameFragment = CatalogFragment.NAME;
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notifications:
                break;
        }
        if(nameFragment != null){
            mRootPresenter.clickOpenFragment(nameFragment);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case ConstantManager.EXIT_DIALOG:
                String[] selectItem = {getString(R.string.exit_dialog_yes), getString(R.string.exit_dialog_cancel)};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.exit_dialog_title));
                builder.setItems(selectItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                break;
                            case 1:
                                dialog.cancel();
                                break;
                        }
                    }
                });
                return builder.create();
            default:
                return null;
        }
    }

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerRootActivity_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void setMenuItemCheckedByNameFragment(String fragmentName){
        MenuItem menuItem = null;
        switch(fragmentName){
            case CatalogFragment.NAME:
                menuItem = mNavigationView.getMenu().findItem(R.id.nav_catalog);
                break;
            case ProfileFragment.NAME:
                menuItem = mNavigationView.getMenu().findItem(R.id.nav_account);
                break;
        }
        if(menuItem != null){
            menuItem.setChecked(true);
        }
    }

    //region ================= IRootView =================
    @Override
    public void setTitleByNameFragment(String fragmentName){
        ActionBar aCtionBar = getSupportActionBar();
        if(aCtionBar != null){
            switch(fragmentName){
                case CatalogFragment.NAME:
                    aCtionBar.setTitle(R.string.catalog_fragment_title);
                    break;
                case ProfileFragment.NAME:
                    aCtionBar.setTitle(R.string.profile_fragment_title);
                    break;
                case AuthFragment.NAME:
                    aCtionBar.setTitle(R.string.auth_fragment_title);
                    break;
            }
        }
    }

    @Override
    public void openFragment(Fragment fragment){
        if(!mCurrenFragmentName.equals(fragment)){
            if(fragment != null){
                setTitleByNameFragment(fragment.getClass().getSimpleName());
                setMenuItemCheckedByNameFragment(fragment.getClass().getSimpleName());
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        }
    }

    @Override
    public void setOrderNotification(String quantity) {
        if(mNotifyCount != null){
            mNotifyCount.setVisibility(View.VISIBLE);
            mNotifyCount.setText(quantity);
        }
    }
    //endregion

    //region ================= IActivityView =================
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if(BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        }else{
            showMessage("Что-то пошло не так");
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public void setUserInfoInDrawer(UserProfileDTO userProfileDTO) {
        View view = mNavigationView.getHeaderView(0);
        ImageView userAvatarImg = (ImageView) view.findViewById(R.id.user_avatar_img);
        LoaderImage.getInstanse().setUserAvatar(
                userProfileDTO.getAvatarUrl(), userAvatarImg, 2, Color.WHITE);

        ImageView userCoverImg = (ImageView) view.findViewById(R.id.user_cover_img);
        LoaderImage.getInstanse().setUserCover(userProfileDTO.getCoverUrl(), userCoverImg);

        TextView userFullNameTxt = (TextView) view.findViewById(R.id.user_name_txt);
        userFullNameTxt.setText(userProfileDTO.getFullName());
    }
    //endregion

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @RootScope
        RootPresenter provideProductPresenter(){
            return new RootPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @RootScope
    public interface Component {
        void inject(RootActivity activity);
        RootPresenter gerRootPresenter();
    }

    //endregion
}
