package com.softdesign.mvpauth.ui.adapters;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.data.storage.dto.AddressDTO;
import com.softdesign.mvpauth.utils.ConstantManager;

import java.util.List;

public class AddressAdapter extends BaseAdapter {

    private static final String TAG = ConstantManager.TAG_PREFIX + "AddressAdapter";

    private Context mContext;
    private LayoutInflater mInflater;
    private List<AddressDTO> mAddressList;

    public AddressAdapter(List<AddressDTO> addressList, Context context) {
        mContext = context;
        mAddressList = addressList;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mAddressList.size();
    }

    @Override
    public String getItem(int i) {
        return mAddressList.get(i).getNameDeliveryLocation();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View itemView = view;
        if(itemView == null){
            itemView = mInflater.inflate(R.layout.item_address_list, viewGroup, false);
            Log.d(TAG, "getView");
        }

        TextInputLayout nameAddress = (TextInputLayout) itemView.findViewById(R.id.name_address);
        nameAddress.setHint(mAddressList.get(i).getNameDeliveryLocation());
        EditText address = (EditText) itemView.findViewById(R.id.address_et);
        address.setText(mAddressList.get(i).getStreet());
        EditText comment = (EditText) itemView.findViewById(R.id.comment_et);
        comment.setText(mAddressList.get(i).getComment());

        return itemView;
    }
}
