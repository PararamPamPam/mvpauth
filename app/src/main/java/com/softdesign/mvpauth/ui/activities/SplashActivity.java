package com.softdesign.mvpauth.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.softdesign.mvpauth.BuildConfig;
import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AuthScope;
import com.softdesign.mvpauth.mvp.presenters.AuthActivityPresenter;
import com.softdesign.mvpauth.mvp.views.IAuthActivityView;
import com.softdesign.mvpauth.ui.custom_views.AuthPanel;
import com.softdesign.mvpauth.utils.textwatchers.EmailTextWatchers;
import com.softdesign.mvpauth.utils.textwatchers.PasswordTextWatcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Provides;

public class SplashActivity extends BaseActivity implements IAuthActivityView, View.OnClickListener{

    @Inject
    AuthActivityPresenter mAuthActivityPresenter;

    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;
    @BindView(R.id.app_name_txt)
    TextView mAppnameTxt;
    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    //region ========================= Life cycle ========================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);
        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/PTBebasNeueBook.ttf");
        mAppnameTxt.setTypeface(typeface);

        daggerInject();

        mAuthActivityPresenter.takeView(this);
        mAuthActivityPresenter.initView();

        mShowCatalogBtn.setOnClickListener(this);
        mLoginBtn.setOnClickListener(this);
        mEmailEt.addTextChangedListener(new EmailTextWatchers(mEmailEt));
        mPasswordEt.addTextChangedListener(new PasswordTextWatcher(mPasswordEt));
    }

    @Override
    protected void onDestroy() {
        mAuthActivityPresenter.dropView();
        if(isFinishing()){
            DaggerService.unregisterScope(AuthScope.class);
        }
        super.onDestroy();
    }
    //endregion

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerSplashActivity_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    //region ======================== AuthView ========================

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if(BuildConfig.DEBUG){
            showMessage(e.getMessage());
            e.printStackTrace();
        }else{
            showMessage("Что-то пошло не так");
        }
    }

    @Override
    public void showLoad() {
        showProgress();
    }

    @Override
    public void hideLoad() {
        hideProgress();
    }

    @Override
    public void setOrderNotification(String quantity) {
    }

    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.GONE);
    }

    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }

    @Override
    public void showCatalogScreen() {
        Intent intent = new Intent(this, RootActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public EditText getEmailEditText() {
        return mEmailEt;
    }

    @Override
    public EditText getPasswordEditText() {
        return mPasswordEt;
    }
    //endregion


    @Override
    public void onBackPressed() {
        if(!mAuthPanel.isIdle()) {
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.login_btn:
                mAuthActivityPresenter.clickOnLogin();
                break;
            case R.id.show_catalog_btn:
                mAuthActivityPresenter.clickOnShowCaralog();
                break;
        }
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @AuthScope
        AuthActivityPresenter provideAuthActivityPresenter(){
            return new AuthActivityPresenter();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {
        void inject(SplashActivity activity);
    }

    //endregion

}
