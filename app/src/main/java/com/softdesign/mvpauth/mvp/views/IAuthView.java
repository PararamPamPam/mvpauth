package com.softdesign.mvpauth.mvp.views;

import android.support.annotation.Nullable;
import android.widget.EditText;

import com.softdesign.mvpauth.ui.custom_views.AuthPanel;

public interface IAuthView extends IView{

    @Nullable
    AuthPanel getAuthPanel();

    EditText getEmailEditText();
    EditText getPasswordEditText();
}
