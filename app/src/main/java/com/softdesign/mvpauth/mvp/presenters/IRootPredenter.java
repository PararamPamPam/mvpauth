package com.softdesign.mvpauth.mvp.presenters;

public interface IRootPredenter {

    void clickOpenFragment(String nameFragment);
    void setupMenu();
}
