package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.data.storage.dto.ProductDTO;

import java.util.HashMap;
import java.util.Map;

public class ProductPresenterFactory {

    private static final Map<String, ProductPresenter> sPresenterMap =
            new HashMap<String, ProductPresenter>();

    private static void registerPresenter(ProductDTO productDTO, ProductPresenter presenter){
        sPresenterMap.put(String.valueOf(productDTO.getId()), presenter);
    }

    public static ProductPresenter getInstance(ProductDTO productDTO){
        ProductPresenter presenter = sPresenterMap.get(String.valueOf(productDTO.getId()));
        if(presenter == null){
            presenter = new  ProductPresenter(productDTO);
            registerPresenter(productDTO, presenter);
        }
        return presenter;
    }
}
