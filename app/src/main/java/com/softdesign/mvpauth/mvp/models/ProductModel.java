package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.managers.DataManager;
import com.softdesign.mvpauth.data.storage.dto.ProductDTO;

public class ProductModel extends AbstractModel{

    public ProductDTO getProductById(int productId){
        return mDataManager.getProductById(productId);
    }

    public void updateProduct(ProductDTO product){
        mDataManager.updateProduct(product);
    }
}
