package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.storage.dto.ProductDTO;

import java.util.List;

public class CatalogModel extends AbstractModel{

    public List<ProductDTO> getProductList(){
        return mDataManager.getProductList();
    }

    public void addOrder(int orderId, int quantity){
        mDataManager.addOrder(orderId, quantity);
    }

}
