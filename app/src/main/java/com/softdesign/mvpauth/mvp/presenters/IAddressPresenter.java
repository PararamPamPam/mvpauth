package com.softdesign.mvpauth.mvp.presenters;

public interface IAddressPresenter {

    void clickOnPointOnMap();
    void clickOnAddAddressDelivery();
    void setupMenu();
}
