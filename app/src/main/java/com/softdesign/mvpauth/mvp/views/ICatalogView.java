package com.softdesign.mvpauth.mvp.views;

import com.softdesign.mvpauth.data.storage.dto.ProductDTO;

import java.util.List;

public interface ICatalogView extends IView {
    void showCatalogView(List<ProductDTO> productList);
    void showAuthScreen();
    void updateProductCounter();
}
