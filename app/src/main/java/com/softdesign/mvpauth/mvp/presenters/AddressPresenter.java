package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.R;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AddressScope;
import com.softdesign.mvpauth.mvp.models.AddressModel;
import com.softdesign.mvpauth.mvp.views.IAddressView;
import com.softdesign.mvpauth.utils.App;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class AddressPresenter extends AbstractPresenter<IAddressView> implements IAddressPresenter{

    @Inject
    AddressModel mAddressModel;

    public AddressPresenter() {
        daggerInject();
    }

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerAddressPresenter_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
    }

    @Override
    public void clickOnPointOnMap() {
    }

    @Override
    public void clickOnAddAddressDelivery() {
        if(getView() != null){
            if(getView().checkInputData()){
                mAddressModel.saveAddressDelivery(getView().getAddressDelivery());
                getView().close();
            }else{
                getView().showMessage(App.getContext()
                        .getResources().getString(R.string.incorrect_data_address));
            }
        }
    }

    @Override
    public void setupMenu() {
        if(getView() != null){
            int countOrder = mAddressModel.getCountOrder();
            if(mAddressModel.isAuthUser() && countOrder > 0){
                getView().setOrderNotification(String.valueOf(countOrder));
            }
        }
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @AddressScope
        AddressModel provideAddressModel(){
            return new AddressModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @AddressScope
    interface Component {
        void inject(AddressPresenter presenter);
    }
    //endregion
}
