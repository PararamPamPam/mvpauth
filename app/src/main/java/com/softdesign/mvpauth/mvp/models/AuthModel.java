package com.softdesign.mvpauth.mvp.models;

public class AuthModel extends AbstractModel{

    public void loginUser(String email, String password){
        mDataManager.loginUser(email, password);
    }
}
