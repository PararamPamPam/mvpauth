package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.data.storage.dto.ProductDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.ProductScope;
import com.softdesign.mvpauth.mvp.models.ProductModel;
import com.softdesign.mvpauth.mvp.views.IProductView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter{

    private static final String TAG = "ProductPresenter";

    @Inject
    ProductModel mProductModel;
    private ProductDTO mProduct;

    public ProductPresenter(ProductDTO productDTO) {
        daggerInject();
        mProduct = productDTO;
    }

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerProductPresenter_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void clickOnPlus() {
        mProduct.addProduct();
        mProductModel.updateProduct(mProduct);
        if(getView() != null){
            getView().updateProductCountView(mProduct);
        }
    }

    @Override
    public void clickOnMinus() {
        if(mProduct.getCount() > 0){
            mProduct.deleteProduct();
            mProductModel.updateProduct(mProduct);
            if(getView() != null){
                getView().updateProductCountView(mProduct);
            }
        }
    }

    @Override
    public void initView() {
        if(getView() != null){
            getView().showProductView(mProduct);
        }
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @ProductScope
        ProductModel provideProductModel(){
            return new ProductModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @ProductScope
    interface Component {
        void inject(ProductPresenter presenter);
    }

    private Component createDaggerComponent(){
        return DaggerProductPresenter_Component.builder()
                .module(new Module())
                .build();
    }

    //endregion
}
