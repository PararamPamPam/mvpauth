package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.managers.DataManager;
import com.softdesign.mvpauth.data.storage.dto.UserProfileDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.components.DaggerModelComponent;
import com.softdesign.mvpauth.di.components.ModelComponent;
import com.softdesign.mvpauth.di.modules.ModelModule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public abstract class AbstractModel {

    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        daggerInject();
    }

    private void daggerInject(){
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new ModelModule());

            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerModelComponent.class);
            DaggerService.registertComponent(ModelComponent.class, component);
        }
        component.inject(this);
    }

    public boolean isAuthUser(){
        return mDataManager.isAuthUser();
    }

    public int getCountOrder(){
        return mDataManager.getCountOrders();
    }

    public UserProfileDTO getUserProfileInfo() {
        return mDataManager.getUserProfileInfo();
    }

}
