package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.storage.dto.UserProfileDTO;

public class ProfileModel extends AbstractModel{
    public UserProfileDTO getUserProfileInfo() {
        return mDataManager.getUserProfileInfo();
    }
}
