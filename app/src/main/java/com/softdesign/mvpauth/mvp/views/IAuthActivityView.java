package com.softdesign.mvpauth.mvp.views;

public interface IAuthActivityView extends IActivityView, IAuthView{

    void showLoginBtn();
    void hideLoginBtn();
    void showCatalogScreen();
}
