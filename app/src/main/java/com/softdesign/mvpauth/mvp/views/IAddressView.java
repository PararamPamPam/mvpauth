package com.softdesign.mvpauth.mvp.views;

import com.softdesign.mvpauth.data.storage.dto.AddressDTO;

public interface IAddressView extends IActivityView{
    AddressDTO getAddressDelivery();
    boolean checkInputData();
    void close();
}
