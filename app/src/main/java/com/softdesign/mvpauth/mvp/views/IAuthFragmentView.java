package com.softdesign.mvpauth.mvp.views;

public interface IAuthFragmentView extends IAuthView{

    void goBack();
    String getNextFragmetnName();
}
