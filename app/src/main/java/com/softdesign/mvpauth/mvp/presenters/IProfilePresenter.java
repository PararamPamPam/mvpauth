package com.softdesign.mvpauth.mvp.presenters;

public interface IProfilePresenter {

    void clickAddAddressBtn();
    void swithNoticeAboutStatusOrder(boolean state);
    void swithNoticeAboutPrmotions(boolean state);
    void resume();
}
