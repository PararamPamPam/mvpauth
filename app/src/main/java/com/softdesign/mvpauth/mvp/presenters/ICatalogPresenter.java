package com.softdesign.mvpauth.mvp.presenters;

public interface ICatalogPresenter {
    void clickOnBuyButton(int position);
}
