package com.softdesign.mvpauth.mvp.views;

public interface IActivityView extends IView {
    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();

    void setOrderNotification(String quantity);
}
