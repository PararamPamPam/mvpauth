package com.softdesign.mvpauth.mvp.presenters;

import android.widget.EditText;

import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AuthScope;
import com.softdesign.mvpauth.mvp.models.AuthModel;
import com.softdesign.mvpauth.mvp.views.IAuthView;
import com.softdesign.mvpauth.ui.custom_views.AuthPanel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public abstract class AbstractAuthPresenter<T extends IAuthView>
        extends AbstractPresenter<T> implements IAuthPresenter {

    protected AuthModel mAuthModel;

    public AbstractAuthPresenter() {
        mAuthModel = new Creator().mAuthModel;
    }

    public abstract void showLoad();
    public abstract void hideLoad();
    public abstract void showMessage(String message);
    public abstract void showError(Throwable e);

    @Override
    public void clickOnLogin() {
        if(getView() != null && getView().getAuthPanel() != null){
            if(getView().getAuthPanel().isIdle()){
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            }else{
                if(false){
                    showMessage("Введены некорректные данные");
                }else{
                    mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(),
                            getView().getAuthPanel().getUserPassword());
                    entry();
                }
            }
        }
    }

    protected abstract void entry();

    @Override
    public void clickOnVk() {
        if(getView() != null){
            showMessage("clickOnVk");
        }
    }

    @Override
    public void clickOnFb() {
        if(getView() != null){
            showMessage("clickOnFb");
        }
    }

    @Override
    public void clickOnTwitter() {
        if(getView() != null){
            showMessage("clickOnTwitter");
        }
    }

    public boolean isCorrectInputData() {
        if(getView() != null){
            EditText emailEt = getView().getEmailEditText();
            EditText loginEt = getView().getPasswordEditText();

            if(emailEt.getError() == null && !emailEt.getText().toString().equals("") &&
                    loginEt.getError() == null && !loginEt.getText().toString().equals("")){
                return true;
            }
        }
        return false;
    }

    //TODO придумать нормальное название для класса
    protected static class Creator{

        @Inject
        AuthModel mAuthModel;

        public Creator() {
            daggerInject();
        }

        private void daggerInject(){
            Component component = DaggerService.getComponent(Component.class);
            if(component == null){
                List<Object> modulesAndComponentList = new ArrayList<>();
                Collections.addAll(modulesAndComponentList,
                        new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerAbstractAuthPresenter_Creator_Component.class);
                DaggerService.registertComponent(Component.class, component);
            }
            component.inject(this);
        }

        //region =================== DI ===================

        @dagger.Module
        public class Module {
            @Provides
            @AuthScope
            AuthModel provideAuthModel(){
                return new AuthModel();
            }
        }

        @dagger.Component(modules = Module.class)
        @AuthScope
        interface Component {
            void inject(Creator creater);
        }

        //endregion
    }

}
