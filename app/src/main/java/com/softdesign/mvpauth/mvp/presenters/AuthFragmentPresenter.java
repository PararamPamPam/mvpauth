package com.softdesign.mvpauth.mvp.presenters;

import android.util.Log;

import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AuthScope;
import com.softdesign.mvpauth.mvp.views.IAuthFragmentView;
import com.softdesign.mvpauth.mvp.views.IAuthView;
import com.softdesign.mvpauth.ui.activities.RootActivity;
import com.softdesign.mvpauth.ui.custom_views.AuthPanel;
import com.softdesign.mvpauth.ui.fragments.AuthFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class AuthFragmentPresenter extends AbstractAuthPresenter<IAuthFragmentView>
        implements IAuthPresenter {

    @Inject
    RootPresenter mRootPresenter;

    public AuthFragmentPresenter() {
        daggerInject();
    }

    private void daggerInject() {
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    DaggerService.getComponent(RootActivity.Component.class));
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerAuthFragmentPresenter_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void showLoad() {
        if(mRootPresenter.getView() != null){
            mRootPresenter.getView().showLoad();
        }
    }

    @Override
    public void hideLoad() {
        if(mRootPresenter.getView() != null){
            mRootPresenter.getView().hideLoad();
        }
    }

    @Override
    public void showMessage(String message) {
        if(mRootPresenter.getView() != null){
            mRootPresenter.getView().showMessage(message);
        }
    }

    @Override
    public void showError(Throwable e) {
        if(mRootPresenter.getView() != null){
            mRootPresenter.getView().showError(e);
        }
    }

    @Override
    protected void entry() {
        if(getView() != null){
            if(mRootPresenter.getView() != null){
                if(mAuthModel.isAuthUser()){
                    mRootPresenter.getView().setUserInfoInDrawer(mAuthModel.getUserProfileInfo());
                }
            }
            if(getView().getNextFragmetnName() == null){
                getView().goBack();
            }else{
                mRootPresenter.clickOpenFragment(getView().getNextFragmetnName());
            }
        }
    }

    @Override
    public void initView() {
        if(getView() != null){
            getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            if(mRootPresenter.getView() != null){
                Log.d("DEV ", "initView AuthFragmentPresenter");
                mRootPresenter.getView().setTitleByNameFragment(AuthFragment.class.getSimpleName());
            }
        }
    }

    //region =================== DI ===================

    @dagger.Component(dependencies = RootActivity.Component.class)
    @AuthScope
    interface Component {
        void inject(AuthFragmentPresenter presenter);
    }
    //endregion
}
