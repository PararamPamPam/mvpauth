package com.softdesign.mvpauth.mvp.models;

import com.softdesign.mvpauth.data.storage.dto.AddressDTO;

public class AddressModel extends AbstractModel{

    public void saveAddressDelivery(AddressDTO addressDTO){
        mDataManager.addAddress(addressDTO);
    }
}
