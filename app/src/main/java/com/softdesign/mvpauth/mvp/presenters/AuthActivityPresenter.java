package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.mvp.views.IAuthActivityView;

public class AuthActivityPresenter extends AbstractAuthPresenter<IAuthActivityView>
        implements IAuthActivityPresenter{

    @Override
    public void initView() {
        if(getView() != null){
            if(mAuthModel.isAuthUser()){
                getView().hideLoginBtn();
            }else{
                getView().showLoginBtn();
            }
        }
    }

    @Override
    public void clickOnShowCaralog() {
        if(getView() != null){
            getView().showCatalogScreen();
        }
    }

    @Override
    public void showLoad() {
        if(getView() != null){
            getView().showLoad();
        }
    }

    @Override
    public void hideLoad() {
        if(getView() != null){
            getView().hideLoad();
        }
    }

    @Override
    public void showMessage(String message) {
        if(getView() != null){
            getView().showMessage(message);
        }
    }

    @Override
    public void showError(Throwable e) {
        if(getView() != null){
            getView().showError(e);
        }
    }

    @Override
    protected void entry() {
        if(getView() != null){
            getView().showCatalogScreen();
        }
    }
}
