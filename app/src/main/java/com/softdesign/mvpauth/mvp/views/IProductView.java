package com.softdesign.mvpauth.mvp.views;

import com.softdesign.mvpauth.data.storage.dto.ProductDTO;

public interface IProductView extends IView{
    void showProductView(ProductDTO product);
    void updateProductCountView(ProductDTO product);
}
