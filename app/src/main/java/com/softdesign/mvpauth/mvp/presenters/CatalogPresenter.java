package com.softdesign.mvpauth.mvp.presenters;

import com.softdesign.mvpauth.data.storage.dto.ProductDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.CatalogScope;
import com.softdesign.mvpauth.mvp.models.CatalogModel;
import com.softdesign.mvpauth.mvp.views.ICatalogView;
import com.softdesign.mvpauth.mvp.views.IRootView;
import com.softdesign.mvpauth.ui.activities.RootActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {

    @Inject
    RootPresenter mRootPresenter;
    @Inject
    CatalogModel mCatalogModel;

    private List<ProductDTO> mProductList;

    public CatalogPresenter() {
        daggerInject();
    }

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    DaggerService.getComponent(RootActivity.Component.class),
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerCatalogPresenter_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        if(mProductList == null){
            mProductList = mCatalogModel.getProductList();
        }
        if(getView() != null){
            getView().showCatalogView(mProductList);
        }
    }

    @Override
    public void clickOnBuyButton(int position) {
        if(getView() != null){
            if(mCatalogModel.isAuthUser()){
                if(mProductList.get(position).getCount() > 0){
                    mCatalogModel.addOrder(mProductList.get(position).getId(),
                            mProductList.get(position).getCount());
                    getRootView().setOrderNotification(String.valueOf(mCatalogModel.getCountOrder()));
                    getRootView().showMessage(
                            "Товар " + mProductList.get(position).getProductName() + " Успешно добавлен в корзину");
                }else{
                    getRootView().showMessage("Вы не указали количество товаров");
                }
            }else {
                getView().showAuthScreen();
            }
        }
    }

    private IRootView getRootView(){
        return mRootPresenter.getView();
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel(){
            return new CatalogModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    @CatalogScope
    interface Component {
        void inject(CatalogPresenter presenter);
    }

    //endregion
}
