package com.softdesign.mvpauth.mvp.views;

import android.app.Activity;
import android.content.Context;

import com.softdesign.mvpauth.data.storage.dto.AddressDTO;
import com.softdesign.mvpauth.data.storage.dto.UserProfileDTO;

import java.util.List;

public interface IProfileView extends IView{
    void openAddAddressScreen(Activity activity);
    void setUserProfileInfo(UserProfileDTO userProfileDTO);
    void setAddress(List<AddressDTO> addressList, Context contex);
}
