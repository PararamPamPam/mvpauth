package com.softdesign.mvpauth.mvp.presenters;

public interface IAuthPresenter {

    void clickOnLogin();
    void clickOnVk();
    void clickOnFb();
    void clickOnTwitter();

}
