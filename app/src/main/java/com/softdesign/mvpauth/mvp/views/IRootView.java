package com.softdesign.mvpauth.mvp.views;

import android.support.v4.app.Fragment;

import com.softdesign.mvpauth.data.storage.dto.UserProfileDTO;

public interface IRootView extends IActivityView{

    void setUserInfoInDrawer(UserProfileDTO userProfileDTO);
    void setTitleByNameFragment(String fragmentName);
    void openFragment(Fragment fragment);
}
