package com.softdesign.mvpauth.mvp.presenters;

import android.app.Activity;
import android.content.Context;

import com.softdesign.mvpauth.data.storage.dto.UserProfileDTO;
import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.ProfileScope;
import com.softdesign.mvpauth.mvp.models.ProfileModel;
import com.softdesign.mvpauth.mvp.views.IProfileView;
import com.softdesign.mvpauth.ui.activities.RootActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class ProfilePresenter extends AbstractPresenter<IProfileView> implements IProfilePresenter{

    @Inject
    RootPresenter mRootPresenter;
    @Inject
    ProfileModel mProfileModel;

    public ProfilePresenter() {
        daggerInject();
    }

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    DaggerService.getComponent(RootActivity.Component.class),
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerProfilePresenter_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        if(getView() != null){
            UserProfileDTO userProfileDTO = mProfileModel.getUserProfileInfo();
            getView().setUserProfileInfo(userProfileDTO);
        }
    }

    @Override
    public void clickAddAddressBtn() {
        if(getView() != null){
            Activity activity = (Activity) mRootPresenter.getView();
            getView().openAddAddressScreen(activity);
        }
    }

    @Override
    public void swithNoticeAboutStatusOrder(boolean state) {
    }

    @Override
    public void swithNoticeAboutPrmotions(boolean state) {
    }

    @Override
    public void resume() {
        if(getView() != null){
            UserProfileDTO userProfileDTO = mProfileModel.getUserProfileInfo();
            getView().setAddress(userProfileDTO.getAddressDTOList(), (Context) mRootPresenter.getView());
        }
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {

        @Provides
        @ProfileScope
        ProfileModel provideProfileModel(){
            return new ProfileModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    @ProfileScope
    interface Component {
        void inject(ProfilePresenter presenter);
    }

    //endregion
}
