package com.softdesign.mvpauth.mvp.presenters;

import android.support.v4.app.Fragment;

import com.softdesign.mvpauth.di.DaggerService;
import com.softdesign.mvpauth.di.scopes.AuthScope;
import com.softdesign.mvpauth.mvp.models.RootModel;
import com.softdesign.mvpauth.mvp.views.IRootView;
import com.softdesign.mvpauth.ui.fragments.AuthFragment;
import com.softdesign.mvpauth.ui.fragments.CatalogFragment;
import com.softdesign.mvpauth.ui.fragments.ProfileFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import dagger.Provides;

public class RootPresenter extends AbstractPresenter<IRootView> implements IRootPredenter{

    @Inject
    RootModel mRootModel;

    public RootPresenter() {
        daggerInject();
    }

    private void daggerInject(){
        Component component = DaggerService.getComponent(Component.class);
        if(component == null){
            List<Object> modulesAndComponentList = new ArrayList<>();
            Collections.addAll(modulesAndComponentList,
                    new Module());
            component = DaggerService.createDaggercomponent(
                    modulesAndComponentList, DaggerRootPresenter_Component.class);
            DaggerService.registertComponent(Component.class, component);
        }
        component.inject(this);
    }

    @Override
    public void initView() {
        if(getView() != null){
            if(mRootModel.isAuthUser()){
                getView().setUserInfoInDrawer(mRootModel.getUserProfileInfo());
            }
        }
    }

    @Override
    public void clickOpenFragment(String nameFragment) {
        if (getView() != null) {
            boolean authUser = mRootModel.isAuthUser();
            Fragment fragment = null;
            switch (nameFragment){
                case CatalogFragment.NAME:
                    fragment = authUser ?
                            new CatalogFragment() : AuthFragment.newInstance(nameFragment);
                    break;
                case ProfileFragment.NAME:
                    fragment = authUser ?
                            new ProfileFragment() : AuthFragment.newInstance(nameFragment);
                    break;
                case AuthFragment.NAME:
                    fragment = new AuthFragment();
                    break;
            }
            if(fragment != null){
                getView().openFragment(fragment);
            }
        }
    }

    @Override
    public void setupMenu() {
        if(getView() != null){
            int countOrder = mRootModel.getCountOrder();
            if(mRootModel.isAuthUser() && countOrder > 0){
                getView().setOrderNotification(String.valueOf(countOrder));
            }
        }
    }

    //region =================== DI ===================

    @dagger.Module
    public class Module {
        @Provides
        @AuthScope
        RootModel provideRootModel(){
            return new RootModel();
        }
    }

    @dagger.Component(modules = Module.class)
    @AuthScope
    interface Component {
        void inject(RootPresenter presenter);
    }

    //endregion
}
